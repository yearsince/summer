import tensorflow 
import glob
import numpy as np
import pandas as pd
import math
import os
import sys
from tensorflow.keras.layers import *
from tensorflow.keras.models import *
from tensorflow.keras.optimizers import *
from tensorflow.keras.datasets import *
from pathlib import Path



MANIFEST_DIR = sys.path[0] + "\\train.csv"

Batch_size = 20
Long = 792
Lens = 640


#把标签转成oneHot
def convert2oneHot(index,Lens):
    hot = np.zeros((Lens,))
    hot[int(index)] = 1
    return(hot)



def xs_gen(path=MANIFEST_DIR,batch_size = Batch_size,train=True,Lens=Lens,flag = True):

    img_list = pd.read_csv(path)
    if train:
        img_list = np.array(img_list)[:Lens]
        print("Found %s train items."%len(img_list))
        print("list 1 is",img_list[0,-1])
        steps = math.ceil(len(img_list) / batch_size)    # 确定每轮有多少个batch
    else:
        img_list = np.array(img_list)[Lens:]
        print("Found %s test items."%len(img_list))
        print("list 1 is",img_list[0,-1])
        steps = math.ceil(len(img_list) / batch_size)    # 确定每轮有多少个batch
    
    if flag :
        while True:
            for i in range(steps):
                batch_list = img_list[i * batch_size : i * batch_size + batch_size]
                np.random.shuffle(batch_list)
                batch_x = np.array([file for file in batch_list[:,1:-1]])
                batch_y = np.array([convert2oneHot(label,10) for label in batch_list[:,-1]])
                yield batch_x, batch_y
    else:
        for i in range(steps):
                batch_list = img_list[i * batch_size : i * batch_size + batch_size]
                np.random.shuffle(batch_list)
                batch_x = np.array([file for file in batch_list[-1:,1:-1]])
                batch_y = np.array([convert2oneHot(label,10) for label in batch_list[:,-1]])
                yield batch_x, batch_y

TEST_MANIFEST_DIR = sys.path[0] + "\\test_data.csv"

def ts_gen(path=TEST_MANIFEST_DIR,batch_size = Batch_size):

    img_list = pd.read_csv(path)

    img_list = np.array(img_list)[:Lens]
    print("Found %s train items."%len(img_list))
    print("list 1 is",img_list[0,-1])
    steps = math.ceil(len(img_list) / batch_size)    # 确定每轮有多少个batch

    for i in range(steps):

        batch_list = img_list[i * batch_size : i * batch_size + batch_size]
        #np.random.shuffle(batch_list)
        batch_x = np.array([file for file in batch_list[:,1:]])
        #batch_y = np.array([convert2oneHot(label,10) for label in batch_list[:,-1]])

        yield batch_x



TIME_PERIODS = 6000
def build_model(input_shape=(TIME_PERIODS,),num_classes=10):
    model = Sequential()
    model.add(Reshape((TIME_PERIODS, 1), input_shape=input_shape))
    model.add(Conv1D(16, 8,strides=2, activation='relu',input_shape=(TIME_PERIODS,1)))

    model.add(Conv1D(16, 8,strides=2, activation='relu',padding="same"))
    model.add(MaxPooling1D(2))

    model.add(Conv1D(64, 4,strides=2, activation='relu',padding="same"))
    model.add(Conv1D(64, 4,strides=2, activation='relu',padding="same"))
    model.add(MaxPooling1D(2))
    model.add(Conv1D(256, 4,strides=2, activation='relu',padding="same"))
    model.add(Conv1D(256, 4,strides=2, activation='relu',padding="same"))
    model.add(MaxPooling1D(2))
    model.add(Conv1D(512, 2,strides=1, activation='relu',padding="same"))
    model.add(Conv1D(512, 2,strides=1, activation='relu',padding="same"))
    model.add(MaxPooling1D(2))

    model.add(GlobalAveragePooling1D())
    model.add(Dropout(0.3))
    model.add(Dense(num_classes, activation='softmax'))
    return(model)



Train = True



if __name__ == "__main__":     

    if Train == True:

 

        train_iter = xs_gen()


        val_iter = xs_gen(train=False)
   

        
        ckpt = tensorflow.keras.callbacks.ModelCheckpoint(
            filepath= r"C:\Users\years\Desktop\learn_some\model\'best_model.{epoch:02d}-{val_loss:.4f}.h5",
            monitor='val_loss', save_best_only=True,verbose=1)

        model = build_model()
        opt = Adam(0.0002)
        model.compile(loss='categorical_crossentropy',
                    optimizer=opt, metrics=['accuracy'])
        print(model.summary())

        model.fit(
                x=train_iter,
                steps_per_epoch=Lens//Batch_size,
                epochs=20,
                initial_epoch=0,
                validation_data = val_iter,
                validation_steps = (Long - Lens)//Batch_size,
                callbacks=[ckpt],
                )

        keras_file = r"C:\Users\years\Desktop\learn_some\model\finishModel.h5"
        model.save(keras_file, save_format="h5")


        # quantize int

        def representative_data_gen():
            for input_value , z in xs_gen(flag=False):
                # input_value = np.expand_dims(input_value, axis=0)
               
                input_value = input_value.astype(np.float32)
                yield [input_value]

        model = tensorflow.keras.models.load_model(keras_file)
        model.input.set_shape(1 + model.input.shape[1:])
        # print(20+model.input.shape[1:])
        # 动态量化 dynamic range quantization
        converter = tensorflow.lite.TFLiteConverter.from_keras_model(model)
        converter.optimizations = [tensorflow.lite.Optimize.DEFAULT]
        converter.representative_dataset = representative_data_gen
        # Ensure that if any ops can't be quantized, the converter throws an error
        converter.target_spec.supported_ops = [tensorflow.lite.OpsSet.TFLITE_BUILTINS_INT8]
        # Set the input and output tensors to uint8 (APIs added in r2.3)
        converter.inference_input_type = tensorflow.uint8
        converter.inference_output_type = tensorflow.uint8
        
        tflite_model = converter.convert()
        
        tflite_file = Path(r"C:/Users/years/Desktop/learn_some/model/finishModel.tflite")
        tflite_file.write_bytes(tflite_model)
        print("convert model to tflite done...")
    #     ######


        # model = tensorflow.keras.models.load_model(keras_file)
        # model.input.set_shape(1 + model.input.shape[1:])
        # converter = tensorflow.lite.TFLiteConverter.from_keras_model(model)
        # tflite_model = converter.convert()
        # tflite_file = Path("C:/Users/years/Desktop/learn_some/model/finishModel.tflite")
        # tflite_file.write_bytes(tflite_model)
        # print("tflite is finish")

    
    # else:
        # keras_file = r"C:\Users\years\Desktop\learn_some\model\finishModel.h5"
        # keras_file_tflite = "C:/Users/years/Desktop/learn_some/model/finishModel.tflite"
        # if os.path.exists(keras_file):

        #     test_iter = ts_gen()
            
        #     model = load_model(keras_file)
        #     pres = model.predict_generator(generator=test_iter,steps=math.ceil(528/Batch_size),verbose=1)
        #     print(pres.shape)
        #     ohpres = np.argmax(pres,axis=1)
        #     print(ohpres.shape)
        #     #img_list = pd.read_csv(TEST_MANIFEST_DIR)
        #     df = pd.DataFrame()
        #     df["id"] = np.arange(1,len(ohpres)+1)
        #     df["label"] = ohpres
        #     df.to_csv(r"C:\Users\years\Desktop\learn_some\submmit.csv",index=None)
        # else:
        #     print("error")





        keras_file_tflite = "C:/Users/years/Desktop/learn_some/model/finishModel.tflite"
        interpreter = tensorflow.lite.Interpreter(model_path=keras_file_tflite)
        interpreter.allocate_tensors()
        input_details = interpreter.get_input_details()
        output_details = interpreter.get_output_details()
        print(input_details)
        print(output_details)














 


