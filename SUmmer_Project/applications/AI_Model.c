 /*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-09-13     years       the first version
 */
#include <rt_ai_network_model.h>
#include <rt_ai.h>
#include <AI_Model.h>

static rt_ai_t model = NULL;


void ai_run_complete(void *arg){
    *(int*)arg = 1;
}

rt_uint8_t AI_app(void *arr){
    rt_err_t result = RT_EOK;
    int ai_run_complete_flag = 0;
    rt_ai_buffer_t *work_buffer = rt_malloc(RT_AI_NETWORK_WORK_BUFFER_BYTES +
                                            RT_AI_NETWORK_IN_TOTAL_SIZE_BYTES +
                                            RT_AI_NETWORK_OUT_TOTAL_SIZE_BYTES);

    // find a registered model handle
    model = rt_ai_find(RT_AI_NETWORK_MODEL_NAME);
    if(!model) {
        rt_kprintf("ai model find err\r\n");
        return -1;
    }

    // init the model and allocate memory
    result = rt_ai_init(model, work_buffer);
    if (result != 0) {
        rt_kprintf("ai init err\r\n");
        return -1;
    }

    // prepare input data
    rt_memcpy(model->input[0], arr, RT_AI_NETWORK_IN_1_SIZE_BYTES);
    result = rt_ai_run(model , ai_run_complete, &ai_run_complete_flag);
    if (result != 0) {
        rt_kprintf("ai model run err\r\n");
        return -1;
    }

    // get output and post-process the output
    int pred_num = 0;
    if(ai_run_complete_flag){
        float *out = (float *)rt_ai_output(model, 0);
        for(int i = 1 ; i < 10 ; i++){
            if(out[i] > out[pred_num]){
                pred_num = i;
            }
        }
    }

    rt_free(work_buffer);
    return pred_num;
}

