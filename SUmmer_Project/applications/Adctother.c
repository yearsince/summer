 /*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-08-10     years       the first version
 */

#include <rtthread.h>
#include <rtdevice.h>
#include "drv_common.h"
#include <onenet.h>
#include <string.h>
#include <AI_Model.h>
#include <file.h>

#define THREAD_PRIORITY         25
#define THREAD_STACK_SIZE       512
#define THREAD_TIMESLICE        10
#define SAMPLE_UART_NAME       "uart1"    /* 串口设备名称 */
//#define REFER_VOLTAGE       330         /* 参考电压 3.3V,数据精度乘以100保留2位小数*/
#define CONVERT_BITS      (1<<16) /* 转换位数为16位 */

static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
void user_file(rt_uint8_t Num,char *data, rt_uint8_t lab);
void AI_user(void);

/* 指向信号量的指针 */
static rt_sem_t dynamic_sem = RT_NULL;
// 上传云端
static rt_thread_t oneNetUp = RT_NULL;
//定时器
static rt_timer_t timer1;
static rt_device_t serial;    /* 串口设备句柄 */
/* 用于接收消息的信号量 */
static struct rt_semaphore rx_sem;


ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;



#define ADC_CONVERTED_DATA_BUFFER_SIZE   ((uint32_t)  3000)   /* Size of array aADCxConvertedData[], Aligned on cache line size */
//h750芯片使用ADC 需指定数组地址，详情查看st官网
ALIGN_32BYTES (static uint16_t aADCxConvertedData[ADC_CONVERTED_DATA_BUFFER_SIZE]) __attribute__((section(".ARM.__at_0x24000000")));
static int n_count = 1,a = 0;
rt_uint8_t Num = 0; // 保存轴承状态，使用全局变量方便使用
//计算平均值
float Adc_aver[3] = {0};
float transover[3000] = {0};
//ADC采集完成标志位
int flag = 0;
//定时器1的入口函数
static void timeout1(void *parameter)
{
    if (flag == 1)
    {
        rt_device_write(serial,0,aADCxConvertedData,480);
        flag = 0;
    }
    else
    {
        rt_kprintf("ADC acquisition failed  \r\n");
    }
    HAL_ADC_Start_DMA(&hadc1,(uint32_t *)aADCxConvertedData,ADC_CONVERTED_DATA_BUFFER_SIZE);
}

/* 接收数据回调函数 */
static rt_err_t uart_input(rt_device_t dev, rt_size_t size)
{
    /* 串口接收到数据后产生中断，调用此回调函数，然后发送接收信号量 */
    rt_sem_release(&rx_sem);
    return RT_EOK;
}


static void serial_thread_entry(void *parameter)
{
    char ch;
    rt_uint8_t read_flag = 0;
    while (1)
    {
        /* 从串口读取一个字节的数据，没有读取到则等待接收信号量 */
        while (rt_device_read(serial, -1, &ch, 5) != 1)
        {
            /* 阻塞等待接收信号量，等到信号量后再次读取数据 */
            rt_sem_take(&rx_sem, RT_WAITING_FOREVER);
        }
        /* 读取到的数据通过串口错位输出 */

        /*  0x01启动定时器输出波形
         *
         *  0x02模型故障诊断，并释放信号量，将诊断结果，以及采集到信号的平均值上传云端
         *
         *
         *  0x05暂停定时器，暂停波形数据的发送
         *
         */

        /*数据采集对应关系
         *  0  正常
         *  1  内圈
         *  2  外圈
         *  3  滚珠
         */
        switch(ch)
        {
            case 0x01:
                if (timer1 != RT_NULL)
                  {
                      rt_timer_start(timer1);
                  }
                break;
            case 0x02:
                AI_user();
                break;
            case 0x05:
                rt_timer_stop(timer1);  //暂停定时器
                break;

            case 0x03:
                read_flag = app_read();
                if (read_flag==0)
                {
                    app_creat();
                    n_count = 1;
                    rt_device_write(serial,0,&ch,sizeof(ch));
                }
                else
                {
                    rt_device_write(serial,0,&read_flag,sizeof(read_flag));
                }
                break;
            case 0x04:
                user_file(10,&ch,0);
                break;
            case 0x06:
                user_file(10,&ch,1);
                break;
            case 0x07:
                user_file(10,&ch,2);
                break;
            case 0x08:
                user_file(10,&ch,3);
                break;
            case 0x09:
                user_file(50,&ch,0);
                break;
            case 0x10:
                user_file(50,&ch,1);
                break;
            case 0x11:
                user_file(50,&ch,2);
                break;
            case 0x12:
                user_file(50,&ch,3);
                break;

        }
    }
}

//云端上传入口函数
static void oneNetUp_entry(void *parameter)
{
    static rt_err_t result;
    while(1)
    {
        //阻塞方式等待信号量的接收
        result = rt_sem_take(dynamic_sem, RT_WAITING_FOREVER);
        if (result == RT_EOK)
        {
            switch(Num)
            {

                case 0:
                    onenet_mqtt_upload_string("state","normal");
                break;
                case 1  :
                case 4  :
                case 7  :
                    onenet_mqtt_upload_string("state","Outer ring fault");
                    break;
                case 2  :
                case 5  :
                case 8  :
                    onenet_mqtt_upload_string("state","Inner ring fault");
                    break;
                case 3  :
                case 6  :
                case 9  :
                    onenet_mqtt_upload_string("state","Ball failure");
                    break;
            }


            //计算平均值之前的求和
            for(int i = 0; i < 3000;)
            {
                 Adc_aver[0] += transover[i++];
                 Adc_aver[1] += transover[i++];
                 Adc_aver[2] += transover[i++];

            }

            //将平均值上传至云平台
            onenet_mqtt_upload_digit("adc_x",Adc_aver[0]/1000);
            onenet_mqtt_upload_digit("adc_y",Adc_aver[0]/1000);
            onenet_mqtt_upload_digit("adc_z",Adc_aver[0]/1000);

            memset(Adc_aver, 0, sizeof(Adc_aver));
        }

    }

}


void AI_user()
{

    HAL_ADC_Start_DMA(&hadc1,(uint32_t *)aADCxConvertedData,ADC_CONVERTED_DATA_BUFFER_SIZE);
    //防止数据采集出现问题进行标志位判断
    if(flag == 1)
    {
        memset(transover, 0, sizeof(transover));
        //对采集到的数据进行处理
        for(int a = 0; a<3000; a++)
        {
            transover[a] = (float)(aADCxConvertedData[a]*(float)3.3/CONVERT_BITS);
        }
        //进行故障诊断
        Num = AI_app(transover);
        //释放信号量，onenet线程获取信号量上传云端
        rt_sem_release(dynamic_sem);
        //串口发送至上位机进行显示
        rt_device_write(serial,0,&Num,sizeof(Num));
        flag = 0;
    }
    else
    {
        HAL_ADC_Start_DMA(&hadc1,(uint32_t *)aADCxConvertedData,ADC_CONVERTED_DATA_BUFFER_SIZE);
        memset(transover, 0, sizeof(transover));
        //对采集到的数据进行处理
       for(int a = 0; a<3000; a++)
       {
           transover[a] = (float)(aADCxConvertedData[a]*(float)3.3/CONVERT_BITS);
       }
       //进行故障诊断
       Num = AI_app(transover);

       //释放信号量，onenet线程获取信号量上传云端
       rt_sem_release(dynamic_sem);
       //串口发送至上位机进行显示
       rt_device_write(serial,0,&Num,sizeof(Num));
       flag = 0;

    }

}



void user_file(rt_uint8_t Num,char *data, rt_uint8_t lab)
{

    for(a = n_count; a < Num + n_count; a++)
{

          HAL_ADC_Start_DMA(&hadc1,(uint32_t *)aADCxConvertedData,ADC_CONVERTED_DATA_BUFFER_SIZE);
         if(flag == 1)
         {

            memset(transover, 0, sizeof(transover));
            for(int i = 0; i<3000; i++)
            {
               transover[i] = (float)(aADCxConvertedData[i]*(float)3.3/CONVERT_BITS);
            }
            app_write(a, transover, lab);


         }
         else
         {
            HAL_ADC_Start_DMA(&hadc1,(uint32_t *)aADCxConvertedData,ADC_CONVERTED_DATA_BUFFER_SIZE);
            memset(transover, 0, sizeof(transover));
            for(int i = 0; i<3000; i++)
            {
               transover[i] = (float)(aADCxConvertedData[i]*(float)3.3/CONVERT_BITS);
            }
            app_write(a, transover, lab);
         }
     }
    n_count = a;
    rt_kprintf("write ok \r\n");
    rt_device_write(serial,0,data,sizeof(*data));

}


int adc_getValue(void)
{

    MX_DMA_Init();
    MX_ADC1_Init();
    HAL_ADCEx_Calibration_Start(&hadc1, ADC_SINGLE_ENDED, ADC_SINGLE_ENDED);  //ADC矫正
    memset(aADCxConvertedData, 0, sizeof(aADCxConvertedData));
    HAL_ADC_Start_DMA(&hadc1,(uint32_t *)aADCxConvertedData,ADC_CONVERTED_DATA_BUFFER_SIZE);

    serial = rt_device_find(SAMPLE_UART_NAME);
    if (!serial)
    {
        rt_kprintf("find %s failed!\n", SAMPLE_UART_NAME);
        return RT_ERROR;
    }
    rt_device_open(serial, RT_DEVICE_FLAG_INT_RX | RT_DEVICE_FLAG_DMA_TX);

    /* 初始化信号量 */
        rt_sem_init(&rx_sem, "rx_sem", 0, RT_IPC_FLAG_FIFO);
    /* 设置接收回调函数 */
        rt_device_set_rx_indicate(serial, uart_input);
    /* 创建 serial 线程 */
    rt_thread_t thread = rt_thread_create("serial", serial_thread_entry, RT_NULL, 2048, 25, 10);
    /* 创建成功则启动线程 */
    if (thread != RT_NULL)
    {
        rt_thread_startup(thread);

    }

    //创建定时器，实现上位机波形显示功能
    timer1 = rt_timer_create("timer1", timeout1,
                             RT_NULL, 1000,
                             RT_TIMER_FLAG_PERIODIC);



//    实现数据上传云端功能
    oneNetUp = rt_thread_create("oneNetUp",
                            oneNetUp_entry, RT_NULL ,
                            2048,
                            THREAD_PRIORITY, THREAD_TIMESLICE);

    if (oneNetUp != RT_NULL)
    {
        rt_thread_startup(oneNetUp);
    }
    dynamic_sem = rt_sem_create("dsem", 0, RT_IPC_FLAG_PRIO);
    if (dynamic_sem == RT_NULL)
    {
        rt_kprintf("create dynamic semaphore failed.\n");
        return -1;
    }
    return 0;
}


MSH_CMD_EXPORT(adc_getValue, adc_getValue );







void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
   /* Invalidate Data Cache to get the updated content of the SRAM on the second half of the ADC converted data buffer: 32 bytes */
  SCB_InvalidateDCache_by_Addr((uint32_t *) &aADCxConvertedData[ADC_CONVERTED_DATA_BUFFER_SIZE/2], ADC_CONVERTED_DATA_BUFFER_SIZE);

  flag = 1;

}


static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream0_IRQn);


}



static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_MultiModeTypeDef multimode = {0};
  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_16B;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.NbrOfConversion = 3;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ConversionDataManagement = ADC_CONVERSIONDATA_DMA_ONESHOT;
  hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc1.Init.LeftBitShift = ADC_LEFTBITSHIFT_NONE;
  hadc1.Init.OversamplingMode = DISABLE;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the ADC multi-mode
  */
  multimode.Mode = ADC_MODE_INDEPENDENT;
  if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  sConfig.OffsetSignedSaturation = DISABLE;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_3;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_5;
  sConfig.Rank = ADC_REGULAR_RANK_3;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

void DMA1_Stream0_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Stream0_IRQn 0 */

  /* USER CODE END DMA1_Stream0_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_adc1);
  /* USER CODE BEGIN DMA1_Stream0_IRQn 1 */

  /* USER CODE END DMA1_Stream0_IRQn 1 */
}

/**
  * @brief This function handles ADC1 and ADC2 global interrupts.
  */
void ADC_IRQHandler(void)
{
  /* USER CODE BEGIN ADC_IRQn 0 */

  /* USER CODE END ADC_IRQn 0 */
  HAL_ADC_IRQHandler(&hadc1);
  /* USER CODE BEGIN ADC_IRQn 1 */

  /* USER CODE END ADC_IRQn 1 */
}




