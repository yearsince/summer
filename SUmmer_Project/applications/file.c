/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-09-24     years       the first version
 */

#include <file.h>
#include <string.h>
#include <dfs_posix.h> /* 当需要使用文件操作时，需要包含这个头文件 */
char test1_string[14000];
char test2_string[42000];
void app_creat()
{
    int fp,offset = 0;

    fp = open("/sdcard/tran.csv", O_WRONLY | O_CREAT | O_TRUNC);
    memset(test1_string,0,sizeof(test1_string));
    if(fp>=0)
    {

        offset = sprintf(test1_string+ offset,"%.*s",sizeof("id,"),"id,");
        for(int i = 1;i<=3000;i++)
        {
            offset += sprintf(test1_string + offset, "%d,", i);
        }
        offset += sprintf(test1_string+ offset,"%.*s",sizeof("lable,"),"lable,");
        test1_string[offset - 1] = '\n';
        write(fp, test1_string, sizeof(test1_string));
        close(fp);
        rt_kprintf("creat ok \r\n");
    }
    else
    {
        rt_kprintf("read err \r\n");
    }
}



void app_write(rt_uint16_t id,float *arr,rt_uint16_t lab)
{

    int fp,offset = 0;


    memset(test2_string,0,sizeof(test2_string));
    fp = open("/sdcard/tran.csv", O_RDWR | O_APPEND);
    if(fp>=0)
    {
           offset += sprintf(test2_string + offset, "%d,", id);
           for(int i = 1;i<=3000;i++)
          {
              offset += sprintf(test2_string + offset, "%.10f,", arr[i-1]);
          }
           offset += sprintf(test2_string + offset, "%d,", lab);
          test2_string[offset - 1] = '\n';
          write(fp, test2_string, sizeof(test2_string));
          close(fp);
   }
    else
    {
        rt_kprintf("read err \r\n");

    }
}

rt_uint8_t app_read()
{
    int fd, size;
    char buf;
    fd = open("/sdcard/tran.csv", O_RDONLY | O_CREAT);
    if (fd>= 0)
    {
        size = read(fd, &buf, sizeof(buf));
        close(fd);
        if (size < 0)
        {
            return -1;

        }

    }
    else
    {
        return -1;
    }
    return size;
}


