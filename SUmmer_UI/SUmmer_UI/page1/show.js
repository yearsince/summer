﻿var page = {

    /* 此方法在第一次显示窗体前发生 */
    onLoad: function (event) {
       var ctx = pm.createCanvasContext('Canvas2', this);

        ctx.setFillStyle("#000000");
        ctx.setTextBaseline('center');
        ctx.moveTo(0,10);
        ctx.lineTo(20,10);
        ctx.text(" : X");
        ctx.stroke();
        
        ctx.setFillStyle("#FF0000");
        ctx.setTextBaseline('center');
        ctx.moveTo(0,23);
        ctx.lineTo(20,23);
        ctx.text(" : Y");
        ctx.stroke();

        ctx.setFillStyle("#0000FF");
        ctx.setTextBaseline('center');
        ctx.moveTo(0,36);
        ctx.lineTo(20,36);
        ctx.text(" : Z");
        ctx.stroke();
        
        ctx.draw();
    },

    /* 此方法展示窗体后发生 */
    onResume: function (event) {

    },

    /* 当前页状态变化为显示时触发 */
    onShow: function (event) {
        currentData=this;
    },

    /* 当前页状态变化为隐藏时触发 */
    onHide: function (event) {

    },
// first_show : function(event){
    //     var ctx = pm.createCanvasContext('Canvas1', this)
    //     // 绘制直线与数值的参数
    //     ctx.setFillStyle('black')
    //     ctx.setLineWidth(1)
    //     ctx.setFontSize(16)
    //     ctx.setTextAlign('center')
    //     ctx.setTextBaseline('center')

    //     // 画出x轴
    //     ctx.moveTo(75,240) //原点坐标
    //     ctx.lineTo(445,240)

    //     //画出y轴
    //     ctx.moveTo(75,240)  //原点坐标
    //     ctx.lineTo(75,20)

    //     //y轴箭头制作
    //     ctx.moveTo(68,28)
    //     ctx.lineTo(75,18)
    //     ctx.lineTo(82,28)

    //     //x轴箭头制作
    //     ctx.moveTo(435,233)
    //     ctx.lineTo(445,240)
    //     ctx.lineTo(435,247)

            
    //     //画出x刻度 1个刻度50像素
    //     for( var i = 75; i <= 450 ; i += 50 )
    //     {

    //         ctx.moveTo(i,238)
    //         ctx.lineTo(i,242)
            
    //         ctx.moveTo(i,255)
    //         ctx.text(String(i-75)) 
            
    //     }

    //     //画出y刻度 1个刻度20像素
    //     for( var i = 40, a = 10 ; i < 240 ; i += 20 )
    //     {
    //         ctx.moveTo(73,i)
    //         ctx.lineTo(77,i)

    //         ctx.moveTo(49,i)
    //         ctx.text(String(7000*a))

    //         a -- 
    //     }

    
    //      // 导出路径
    //     ctx.draw()
// },

    /* 此方法关闭窗体前发生 */
    onExit: function (event) {

    },
    uartUpdate: function (event) {

        var point_x = [];
        var point_y = [];
        var point_z = [];
        var that = this;
        console.log("this is uartUpdate");


        
        for(var i = 0 , a = 0; i < event.length -2;a++ )
        {
            point_x[a] = (event.readUInt16LE(i)*200/70000);
            i+=2;

            point_y[a] = (event.readUInt16LE(i)*200/70000);
            i+=2;

            point_z[a] = (event.readUInt16LE(i)*200/70000);
            i+=2;
            
            
        }

   
        function move(pro) {

            var ctx = pm.createCanvasContext('Canvas1', that);
            pro = pro*100;




            ctx.setFillStyle("#000000");
            ctx.moveTo(75,240-point_x[0]);
            for (var i = 1 ; i < pro; i++) {
                 if (i < point_x.length) {
                    ctx.lineTo(i*4+75,240-point_x[i]);
                 }
            }
            ctx.stroke();
            ctx.setFillStyle("#FF0000");
            ctx.moveTo(75,240-point_y[0]);
            for (var i = 1 ; i < pro; i++) {
                 if (i < point_y.length) {
                    ctx.lineTo(i*4+75,240-point_y[i]);
                 }
            }
            ctx.stroke();
            ctx.setFillStyle("#0000FF");
            ctx.moveTo(75,240-point_z[0]);
            for (var i = 1 ; i < pro; i++) {
                 if (i < point_z.length) {
                    ctx.lineTo(i*4+75,240-point_z[i]);
                 }
            }



            // ctx.stroke();
            ctx.draw();
        }

        // console.dir(point_x.length+point_y.length+point_z.length);

        var start = null;
        //动画渲染调用
        function renderCallback(timestamp) {
            if (!start)
                start = timestamp;
            var progress = (timestamp - start) / 1000;
            
            if (progress <= 0.999) {
                move(progress);
            }

            if (0.999 < progress) {
                start = timestamp;
                move(progress);
            }

            if (progress < 1) {
                requestAnimationFrame(renderCallback);
            }
        }
        requestAnimationFrame(renderCallback); // start first frame


         
    },
};

Page(page);

page = 0;
