﻿var page = {
    timer : 0,     
    progress : 1,
    Start : 0,
    Num : 0,
    /* 此方法在第一次显示窗体前发生 */
    onLoad: function () {
         var ctx = pm.createCanvasContext('Canvas1', this);
         ctx.setSourceImage("file.png");
    
    },

    /* 此方法展示窗体后发生 */
    onResume: function (event) {

    },

    /* 当前页状态变化为显示时触发 */
    onShow: function (event) {
         currentData=this;

        
    },

    /* 当前页状态变化为隐藏时触发 */
    onHide: function (event) {

    },

    /* 此方法关闭窗体前发生 */
    onExit: function (event) {

    },
        


    btn : function(event){
        if (uart)    
        {        
            uart.write(new Buffer("01","hex"));
        }
        pm.navigateTo('show/show');
    },


    btn2 : function(event){
      
        pm.navigateTo('diag/diag');
    },

    btn3 : function(event){
      
        pm.navigateTo('collec/collec');
    },
};

Page(page);

page = 0;
