﻿var page = {
    timer : 0,    
    progress : 1,
    /* 此方法在第一次显示窗体前发生 */
    onLoad: function (event) {
        
    },

    /* 此方法展示窗体后发生 */
    onResume: function (event) {

    },

    /* 当前页状态变化为显示时触发 */
    onShow: function (event) {
        currentData=this;
    },

    /* 当前页状态变化为隐藏时触发 */
    onHide: function (event) {

    },

    /* 此方法关闭窗体前发生 */
    onExit: function (event) {

    },
    btn2 : function(event){
        if (uart)    
        {
            uart.write(new Buffer("02","hex"));
        }
        else
        {
            this.setData({label1 : {value : "串口打开失败 !!!"}});
        }
         
    },

     uartUpdate: function (event) {
        var that = this;
        var Num = Number( event.readUInt8(0));

        that.timer = setInterval(function()
        {                          
            if(that.progress > 100) 
            {                 
                /*更新下载完成的文本提示，清除定时器*/   
                clearInterval(that.timer);         
                     
                that.setData({progressbar1 : {hide : true}});   

                that.progress = 0;


                switch(Num)
                {
                    case 1  :
                    case 4  :
                    case 7  :
                        that.setData({label1 : {value : "外圈故障，故障代码："+Num}});
                        break;
                    case 2  :
                    case 5  :
                    case 8  :
                        that.setData({label1 : {value : "内圈故障，故障代码："+Num}});
                        break;
                    case 3  :
                    case 6  :
                    case 9  :
                        that.setData({label1 : {value : "滚珠故障，故障代码："+Num}});
                        break;

                    default: 
                        that.setData({label1 : {value : "正常"}});
                        break;

                }
    
            } 
            else
            {                 
                /*更新progressbar的进度和label的显示文本*/ 
                that.setData({label1 : {value : "诊断中... "+ that.progress + "%"}});                
                that.setData({progressbar1 : {value : that.progress}});                
                that.progress++;            
            }  
                        
        },15)
        

                
     },

    btn1 : function(event){
        if(this.timer)
        {
            clearInterval(this.timer);   
        }
            
         pm.navigateBack();
    },
};

Page(page);

page = 0;
